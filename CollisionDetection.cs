using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollisionDetection : MonoBehaviour
{
    
    public Text LogCollsiionEnter;
    public Text LogCollisionStay;
    public Text LogCollisionExit;

    // //Detect collisions between the GameObjects with Colliders attached
    // void OnCollisionEnter(Collision collision)
    // {
    //     Debug.Log("Collision with " + collision.gameObject.name);
    //     //Check for a match with the specified name on any GameObject that collides with your GameObject
    //     if (collision.gameObject.tag == "Gear_2_t10")
    //     {
    //         //If the GameObject's name matches the one you suggest, output this message in the console
    //         Debug.Log("Do something here");
    //     }

    //     //Check for a match with the specific tag on any GameObject that collides with your GameObject
    //     if (collision.gameObject.tag == "MyGameObjectTag")
    //     {
    //         //If the GameObject has the same tag as specified, output this message in the console
    //         Debug.Log("Do something else here");
    //     }
    // }

    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log("Collision with!");
        LogCollsiionEnter.text = "On Collision Enter: " + collision.collider.name;
    }

    private void OnCollisionStay(Collision collision)
    {
        Debug.Log("Collision with!");
        LogCollisionStay.text = "On Collision stay: " + collision.collider.name;
    }

    private void OnCollisionExit(Collision collision)
    {
        Debug.Log("Collision with!");
        LogCollisionExit.text = "On Collision exit: " + collision.collider.name;
    }
}