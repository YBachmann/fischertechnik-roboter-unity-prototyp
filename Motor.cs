using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Motor : MonoBehaviour
{
    public float turnSpeed = 150f;
    public int motorID = 0;

    public Axle axle;

    // Start is called before the first frame update
    void Start()
    {

    }

    private void Update()
    {
        float rotation = 0;
        if(motorID == 0)
        {
            if (Input.GetKey(KeyCode.I))
                rotation = turnSpeed * Time.deltaTime;

            if (Input.GetKey(KeyCode.O))
                rotation = -turnSpeed * Time.deltaTime;
        }
        else if(motorID == 1)
        {
            if (Input.GetKey(KeyCode.J))
                rotation = turnSpeed * Time.deltaTime;

            if (Input.GetKey(KeyCode.K))
                rotation = -turnSpeed * Time.deltaTime;
        }
        else if(motorID == 2)
        {
            if (Input.GetKey(KeyCode.N))
                rotation = turnSpeed * Time.deltaTime;

            if (Input.GetKey(KeyCode.M))
                rotation = -turnSpeed * Time.deltaTime;
        }

        if (axle != null)
            axle.moveByDegrees(rotation);
    }
}
