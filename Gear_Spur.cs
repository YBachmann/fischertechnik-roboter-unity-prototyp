using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gear_Spur : Gear
{
    public float toothCount;

    // Start is called before the first frame update
    void Start()
    {}

    // Update is called once per frame
    void Update()
    {}

    public override void moveByTooths(float tooths)
    {
        if(flipDirection)
            tooths = -tooths;
        float rotation = 360 * (tooths/toothCount);
        transform.Rotate(transform.forward * rotation);
        if (childGear != null)
            childGear.moveByTooths(-tooths);
        if (childAxle != null)
            childAxle.moveByDegrees(rotation);
    }
    
    public override void moveByDegrees(float degrees)
    {
        if(flipDirection)
            degrees = -degrees;
        float tooths = degrees/360 * toothCount;
        // transform.Rotate(transform.forward * degrees);
        // transform.Rotate(0, 0, degrees);
        transform.RotateAround(transform.position, transform.forward, degrees);
        if (childGear != null)
            childGear.moveByTooths(-tooths);
        if (childAxle != null)
            childAxle.moveByDegrees(degrees);
    }
}
