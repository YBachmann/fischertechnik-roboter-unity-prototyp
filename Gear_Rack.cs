using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gear_Rack : Gear
{
    public double module;
    private double pitch;

    // Start is called before the first frame update
    void Start()
    {
        pitch = module / Math.PI;
        
        if (childAxle != null)
            Debug.Log("Gear of type Gear_Rack can not have a childAxle!");
    }

    // Update is called once per frame
    void Update()
    {}
    
    public override void moveByTooths(float tooths)
    {
        if(flipDirection)
            tooths = -tooths;
        float distance = (float)(tooths * pitch);
        transform.Translate(Vector3.right * distance);
        if (childGear != null)
            childGear.moveByTooths(-tooths);
    }
    
    public override void moveByDegrees(float degrees)
    {
        Debug.Log("Gear of type Gear_Rack can not move by degrees!");
    }
}
