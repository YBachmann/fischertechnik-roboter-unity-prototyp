using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Axle : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    { }

    // Update is called once per frame
    void Update()
    { }

    public Gear childGear;
    public Axle childAxle;

    public void moveByDegrees(float degrees)
    {
        // transform.Rotate(transform.forward * degrees);
        transform.Rotate(0, 0, degrees);
        if (childGear != null)
            childGear.moveByDegrees(degrees);
        if (childAxle != null)
            childAxle.moveByDegrees(degrees);
    }
}