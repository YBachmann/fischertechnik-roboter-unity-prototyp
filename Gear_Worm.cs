using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gear_Worm : Gear
{
    public float toothCount;

    // Start is called before the first frame update
    void Start()
    {}

    // Update is called once per frame
    void Update()
    {}

    public override void moveByTooths(float tooths)
    {
        float rotation = 360 * tooths;
        transform.Rotate(transform.forward * rotation);
        if (childGear != null)
            childGear.moveByTooths(-tooths);
        if (childAxle != null)
            childAxle.moveByDegrees(rotation);
    }
    
    public override void moveByDegrees(float degrees)
    {
        degrees = -degrees; //quick hack
        
        float tooths = degrees/360;
        // transform.Rotate(transform.forward * degrees);
        transform.Rotate(degrees, 0, 0);
        if (childGear != null)
            childGear.moveByTooths(-tooths);
        if (childAxle != null)
            childAxle.moveByDegrees(degrees);
    }
}
