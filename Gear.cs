using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Gear : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {}

    // Update is called once per frame
    void Update()
    {}

    public Gear childGear;
    public Axle childAxle;

    public bool flipDirection = false;

    public abstract void moveByTooths(float tooths);
    public abstract void moveByDegrees(float degrees);
}